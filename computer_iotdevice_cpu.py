import requests # http://docs.python-requests.org/en/master/
import psutil   # https://pypi.python.org/pypi/psutil
import time, sys, platform

hostiotmms = 'https://iotmmsp2000160740trial.hanatrial.ondemand.com/'
apiiotmmsdata = '/com.sap.wss:/iotmmsp2000160740trial.hanatrial.ondemand.com/com.sap.iotservices.mms/v1/api/http/data/'
msgtypeid = 'c89350671ac8e1c4ec17'

deviceid = '5c967ddc-7629-486c-9580-a40336bec4c7'
authtoken = '7ceb869ba8cba06fbbc94988632caa'

url = "https://iotmmsp2000160740trial.hanatrial.ondemand.com/com.sap.iotservices.mms/v1/api/http/data/5c967ddc-7629-486c-9580-a40336bec4c7/"

def readsensors():
	global d_pctCPU
	d_pctCPU = psutil.cpu_percent(percpu=False, interval = 1)
	return

def postiotneo ():
	global d_pctCPU

	s_pctCPU = str(d_pctCPU)
	d_tstamp = int(round(time.time()))

	s_tstamp = str("Daniela")

	print("\nValues to post: ", d_pctCPU, d_tstamp)

	payload = "{\"mode\":\"sync\",\"messageType\":\""+msgtypeid+"\",\"messages\":[{\"cpu_usage\":"+s_pctCPU+",\"cpu_type\":\"generic\",\"_time\":"+s_tstamp+"}]}"
	
	headers = {
			'content-type': "application/json",
			'authorization': "Bearer "+authtoken,
			'cache-control': "no-cache"
			}

	print("Payload to post: ", payload)

	response = requests.request("POST", url, data=payload, headers=headers)

	print(response.status_code, response.text)

	return

try:
	while(True):
		readsensors()
		postiotneo()
		time.sleep(2)
except KeyboardInterrupt:
	print("Stopped by the user!")
	
       
