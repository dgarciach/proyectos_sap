// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#include "DHT.h"

#define DHTPIN2 2     // what pin we're connected to
#define DHTPIN4 4

// Uncomment whatever type you're using!
#define DHTTYPE1 DHT22   // DHT 11 
#define DHTTYPE2 DHT11   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor for normal 16mhz Arduino
DHT dht1(DHTPIN2, DHTTYPE1);
DHT dht2(DHTPIN4, DHTTYPE2);

// NOTE: For working with a faster chip, like an Arduino Due or Teensy, you
// might need to increase the threshold for cycle counts considered a 1 or 0.
// You can do this by passing a 3rd parameter for this threshold.  It's a bit
// of fiddling to find the right value, but in general the faster the CPU the
// higher the value.  The default for a 16mhz AVR is a value of 6.  For an
// Arduino Due that runs at 84mhz a value of 30 works.
// Example to initialize DHT sensor for Arduino Due:
//DHT dht(DHTPIN, DHTTYPE, 30);

void setup() {
  Serial.begin(9600); 
  //Serial.println("DHTxx test!");
 
  dht1.begin();
  dht2.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(1000);

float h1,h2,t1,t2;

h1 = dht1.readHumidity();
t1 = dht1.readTemperature();

h2 = dht2.readHumidity();
t2 = dht2.readTemperature();
  
  // Check if any reads failed and exit early (to try again).
 if (isnan(h1) || isnan(t1)  || isnan(h2) || isnan(t2)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index
  // Must send in temp in Fahrenheit!
  float ha = dht1.computeHeatIndex(t1, h1);
  float hb = dht2.computeHeatIndex(t2, h2);


  Serial.print(h1);
  Serial.print(" 939dcaab-4ca2-4641-a689-7e879bc483cc ");
  Serial.print(t1);
  Serial.println(" ceefc014fe36ca37779bd05d34a579e5 ");

  Serial.print(h2);
  Serial.print(" 9cb102fb-c8aa-41a7-bba4-7a2c894cfa07 ");
  Serial.print(t2);
  Serial.println(" 83c48c4a96a366811b9c434646f67d5b ");
}
