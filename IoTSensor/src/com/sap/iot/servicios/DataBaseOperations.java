package com.sap.iot.servicios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.iot.entidad.*;

public class DataBaseOperations {

	private static Logger logger = LogManager.getLogger(DataBaseOperations.class.getName());
	private DataSource dataSource;

	// acceso a los datos que encapsula el JDBC
	public DataBaseOperations(DataSource newDataSource) throws SQLException {
		setDataSource(newDataSource);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource newDataSource) throws SQLException {
		this.dataSource = newDataSource;

	}

	// traer todos los datos de la tabla
	public List<Entidad> getData() throws SQLException {

		String sql = "select * from T_IOT_56DDBB15C3D7FAD6E138 order by G_CREATED desc";
		ArrayList<Entidad> listData = new ArrayList<Entidad>();
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			// ResultSetMetaData rsmd = rs.getMetaData();

			// int numColum = rsmd.getColumnCount();
			while (rs.next()) {
				Entidad listaData = new Entidad();

				listaData.setG_DEVICE(rs.getString(2));
				listaData.setG_CREATED(rs.getTimestamp(3));
				listaData.setC_TEMPERATURA(rs.getString(4));
				listaData.setC_HUMEDAD(rs.getString(5));
				listData.add(listaData);
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al traer datos" + e.getStackTrace());

			logger.log(Level.INFO, " " + e.getMessage());
		}
		return listData;
	}

	// trea los datos de la tabla T_IOT_19AEB87444B24C22CBA1
	public List<SensorLuzMovimiento> getDataLuzMovimiento() throws SQLException {

		String sql = "select * from T_IOT_19AEB87444B24C22CBA1 order by G_CREATED desc";
		ArrayList<SensorLuzMovimiento> listDataLuzMovimiento = new ArrayList<SensorLuzMovimiento>();
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				SensorLuzMovimiento listaDataLuzMovimiento = new SensorLuzMovimiento();

				listaDataLuzMovimiento.setG_DEVICE(rs.getString(2));
				listaDataLuzMovimiento.setG_CREATED(rs.getTimestamp(3));
				listaDataLuzMovimiento.setC_CANTIDADLUZ(rs.getInt(4));
				listaDataLuzMovimiento.setC_EXISTEMOVIMIENTO(rs.getInt(5));
				listDataLuzMovimiento.add(listaDataLuzMovimiento);
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al traer datos" + e.getStackTrace());

			logger.log(Level.INFO, " " + e.getMessage());
		}
		return listDataLuzMovimiento;
	}

	// obtener datos de la temperatura
	public String getTemperatura(String deviceName) throws Exception {

		// T_IOT_FDC44D26FA15F5DFB5E1

		String sql = "select TOP 1 C_TEMPERATURA from T_IOT_56DDBB15C3D7FAD6E138 where G_DEVICE = ? order by G_CREATED desc ";
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, deviceName);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {

				return rs.getString(1);
			} else {
				return "0";
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	// trae la data de la entidad y los valores correspondientes a la humedad
	public String getHumidity(String deviceName) throws Exception {

		// T_IOT_CBA3148C3303411F8EF5
		String sql = "select TOP 1 C_HUMEDAD from T_IOT_56DDBB15C3D7FAD6E138  where G_DEVICE = ?  order by G_CREATED desc";
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, deviceName);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {

				return rs.getString(1);
			} else {
				return "0";
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	// trae la data correspondiente a la temperatura y la calcula por promedio
	// hora
	public List<Entidad> getTemperatureByHour(String deviceName) throws SQLException {

		String sql = "SELECT  hour(G_CREATED) || ':00 ' || extract(day from G_CREATED) ||'/' || extract(month from G_CREATED)  AS HORA,"
				+ " ROUND(AVG(TO_DOUBLE(C_TEMPERATURA)),1) AS TEMPERATURA"
				+ " FROM T_IOT_56DDBB15C3D7FAD6E138  where G_DEVICE = ? group by  G_CREATED order by G_CREATED";
		ArrayList<Entidad> listHora = new ArrayList<Entidad>();
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, deviceName);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Entidad listaData = new Entidad();
				listaData.setHora(rs.getString(1));
				listaData.setC_TEMPERATURA(rs.getString(2));
				listHora.add(listaData);

			}

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al traer datos" + e.getStackTrace());

			logger.log(Level.INFO, " " + e.getMessage());

		} finally {

			if (con != null) {
				con.close();
			}
		}
		return listHora;

	}
	// trae la data correspondiente a la humeadad y la calcula por promedio hora

	public List<Entidad> getHumidityByHour(String deviceName) throws SQLException {

		String sql = "SELECT  hour(G_CREATED) || ':00 ' || extract(day from G_CREATED) ||'/' || extract(month from G_CREATED)  AS HORA,"
				+ " ROUND(AVG(TO_DOUBLE(C_HUMEDAD)),1) AS HUMEDAD"
				+ " FROM T_IOT_56DDBB15C3D7FAD6E138 where G_DEVICE = ? group by  G_CREATED order by G_CREATED";
		ArrayList<Entidad> listHora = new ArrayList<Entidad>();
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, deviceName);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Entidad listaData = new Entidad();
				listaData.setHora(rs.getString(1));
				listaData.setC_HUMEDAD(rs.getString(2));
				listHora.add(listaData);

			}

		} catch (Exception exception) {

			logger.log(Level.ERROR, "Error al traer datos" + exception.getStackTrace());
			logger.log(Level.INFO, " " + exception.getMessage());
		} finally {

			if (con != null) {
				con.close();
			}
		}
		return listHora;
	}

	public List<Calculos> getAhorroConsumo() throws SQLException {

		String sqlSelect = "select CONSUMO,AHORRO  from CALCULOS WHERE DISPOSITIVO_FK= 'c3c1281c-6aa8-4baf-98bc-8ac18b48cb37' ";
		ArrayList<Calculos> listcal = new ArrayList<Calculos>();
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement pstmt = con.prepareStatement(sqlSelect);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {

				Calculos calculos = new Calculos();

				calculos.setAhorro(rs.getDouble(1));
				calculos.setAhorro(rs.getDouble(2));
				listcal.add(calculos);

			}

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al traer datos" + e.getStackTrace());
			logger.log(Level.INFO, " " + e.getMessage());
		}

		finally {
			if (con != null) {
				con.close();
			}

		}
		return listcal;
	}

	// trae la data de los dispositivos correspondientes a la ubicacion

	public List<Dispositivos> getDataDispositivo() throws SQLException {

		String sql = "select * from DISPOSITIVOS WHERE ESTADO = 'TRUE'";
		Connection con = dataSource.getConnection();

		try {

			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			ArrayList<Dispositivos> listDevices = new ArrayList<Dispositivos>();

			while (rs.next()) {
				Dispositivos listaData = new Dispositivos();
				listaData.setID(rs.getString(1));
				listaData.setLATITUD(rs.getString(2));
				listaData.setLONGITUD(rs.getString(3));
				listaData.setESTADO(rs.getString(4));
				listDevices.add(listaData);

			}

			if (con != null) {
				con.close();
			}

			return listDevices;

		} catch (Exception e) {

			logger.log(Level.ERROR, "Error al traer datos" + e.getStackTrace());
			logger.log(Level.INFO, " " + e.getMessage());
			return null;
		}
	}

	// trae la data proveniente de la entidad SensorLuz que hace referencia a la
	// base de datos tabla T_IOT_19AEB87444B24C22CBA1
	public String getDataLuz() throws SQLException {

		String sql = "select TOP 1 C_CANTIDADLUZ from T_IOT_19AEB87444B24C22CBA1 order by G_CREATED desc ";
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			} else {
				return "0";
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public Calculos getConsumo() throws SQLException {

		String sqlConsumo = "SELECT CONSUMO FROM CALCULOS where DiSPOSITIVO_FK='c3c1281c-6aa8-4baf-98bc-8ac18b48cb37'";

		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlConsumo);
			ResultSet rs = pstmt.executeQuery();

			Calculos calculo = new Calculos();
			while (rs.next()) {

				calculo.setConsumo(rs.getDouble(1));
				// calculo.setAhorro(rs.getDouble(2));
				// listCalculos.add(listaCalculos);

			}
			return calculo;

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void actualizarConsumoAhorro(Calculos data) throws SQLException {

		String sqlUpdate = "UPDATE CALCULOS SET CONSUMO = ?,  AHORRO = ? WHERE DISPOSITIVO_FK='c3c1281c-6aa8-4baf-98bc-8ac18b48cb37'";
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setDouble(1, data.getConsumo());
			pstmt.setDouble(2, data.getAhorro());

			pstmt.executeUpdate();

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al actualizar datos" + e.getStackTrace());

			logger.log(Level.INFO, " " + e.getMessage());
		}

	}

	public void agregarDatos(Calculos datos) throws SQLException {

		String sql = "insert into CALCULOS values(?,?,0,'c3c1281c-6aa8-4baf-98bc-8ac18b48cb37')";
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setDouble(1, datos.getConsumo());
			preparedStatement.setDouble(2, datos.getAhorro());

			preparedStatement.executeUpdate();

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public Calculos getAhorro() throws SQLException {

		String sqlConsumo = "SELECT AHORRO FROM CALCULOS where DiSPOSITIVO_FK='c3c1281c-6aa8-4baf-98bc-8ac18b48cb37'";
		// String sqlInsert="insert into CALCULOS
		// values(?,?,0,'c3c1281c-6aa8-4baf-98bc-8ac18b48cb37')";
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlConsumo);
			ResultSet rs = pstmt.executeQuery();

			Calculos calculo = new Calculos();
			while (rs.next()) {

				calculo.setAhorro(rs.getDouble(1));

				// listCalculos.add(listaCalculos);

			}
			return calculo;

		} finally {
			if (con != null) {
				con.close();

			}
		}

	}

	public void agregarAhorro(Calculos ahorro) throws SQLException {

		String sql = "insert into CALCULOS values(0,?,0,'c3c1281c-6aa8-4baf-98bc-8ac18b48cb37')";
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setDouble(2, ahorro.getAhorro());
			preparedStatement.executeUpdate();

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public void editar(Dispositivos dispositivo) throws SQLException {

		String sql = "UPDATE DISPOSITIVOS SET  LATITUD = ?, LONGITUD = ?  WHERE ID = ?;";
		Connection conexion = dataSource.getConnection();
		try {

			PreparedStatement preparedStatement = conexion.prepareStatement(sql);
			preparedStatement.setString(1, dispositivo.getLATITUD());
			preparedStatement.setString(2, dispositivo.getLONGITUD());
			preparedStatement.setString(3, dispositivo.getID());
			preparedStatement.executeUpdate();

		} finally {
			if (conexion != null) {
				conexion.close();
			}
		}

	}

	public void agregarDispositivo(Dispositivos dispositivo) throws SQLException {

		String sql = "insert into DISPOSITIVOS values(?,?,?);";
		Connection con = dataSource.getConnection();
		try {

			PreparedStatement pstm = con.prepareStatement(sql);
			pstm.setString(1, dispositivo.getID());
			pstm.setString(2, dispositivo.getLATITUD());
			pstm.setString(3, dispositivo.getLONGITUD());

			pstm.executeUpdate();

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void eliminarDispositivo(Dispositivos dispositivo) throws SQLException {

		
		String sql = "UPDATE DISPOSITIVOS SET  ESTADO= ?  WHERE ID = ?;";
		Connection conexion = dataSource.getConnection();
		try {

			PreparedStatement preparedStatement = conexion.prepareStatement(sql);
			preparedStatement.setString(1, dispositivo.getESTADO());
			preparedStatement.setString(2, dispositivo.getID());
			preparedStatement.executeUpdate();

		} finally {
			if (conexion != null) {
				conexion.close();
			}
		}
	}

	public Dispositivos getEstado(String id) throws SQLException {

		String sqlEstado = "SELECT ESTADO FROM DISPOSITIVOS where ID= ?";

		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlEstado);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();

			Dispositivos dis = new Dispositivos();
			while (rs.next()) {

				dis.setESTADO(rs.getString(1));

				// listCalculos.add(listaCalculos);

			}
			return dis;

		} finally {
			if (con != null) {
				con.close();

			}
		}

	}

}
