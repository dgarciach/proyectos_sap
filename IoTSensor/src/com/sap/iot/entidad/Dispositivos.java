package com.sap.iot.entidad;

public class Dispositivos {

	private String ID, LATITUD, LONGITUD, ESTADO;

	public String getLONGITUD() {
		return LONGITUD;
	}

	public void setLONGITUD(String lONGITUD) {
		LONGITUD = lONGITUD;
	}

	public String getLATITUD() {
		return LATITUD;
	}

	public void setLATITUD(String lATITUD) {
		LATITUD = lATITUD;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String eSTADO) {
		ESTADO = eSTADO;
	}
	
	
	
}
