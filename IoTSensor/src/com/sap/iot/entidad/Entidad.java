package com.sap.iot.entidad;

import java.io.Serializable;
import java.sql.Timestamp;

/*@Entity
@Table(schema = "SYSTEM", name = "T_IOT_9E8E602F603409491E30")
@NamedQueries({ @NamedQuery(name = "Entidad.findAll", query = "SELECT d FROM Entidad d"),
		@NamedQuery(name = "Entidad.findByCpuUsage", query = "SELECT d FROM Entidad d WHERE d.C_TEPERATURA = :C_TEPERATURA") })
*/
public class Entidad implements Serializable {

	private static final long serialVersionUID = 1L;

	// public static final String FIND_ALL = "DataEntity.findAll";
	// public static final String FIND_BY_CPU = "DataEntity.findByCpuUsage";

	/*
	 * @Column(name = "G_DEVICE") private String G_DEVICE;
	 * 
	 * @Column(name = "G_CREATED") private Timestamp G_CREATED;
	 * 
	 * @Column(name="C_TEPERATURA") private String C_TEPERATURA;
	 * 
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY)
	 * 
	 * @Column(name = "ID_PRUEBA") private int ID_PRUEBA;
	 * 
	 */
	private String G_DEVICE, C_TEMPERATURA, C_HUMEDAD, ID_REGISTRO, hora;

	private Timestamp G_CREATED;

	public Entidad() {
	}

	public String getG_DEVICE() {
		return G_DEVICE;
	}

	public void setG_DEVICE(String g_DEVICE) {
		G_DEVICE = g_DEVICE;
	}

	public String getC_TEMPERATURA() {
		return C_TEMPERATURA;
	}

	public void setC_TEMPERATURA(String c_TEPERATURA) {
		C_TEMPERATURA = c_TEPERATURA;
	}

	public Timestamp getG_CREATED() {
		return G_CREATED;
	}

	public void setG_CREATED(Timestamp g_CREATED) {
		G_CREATED = g_CREATED;
	}
	
	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getC_HUMEDAD() {
		return C_HUMEDAD;
	}

	public void setC_HUMEDAD(String c_HUMEDAD) {
		C_HUMEDAD = c_HUMEDAD;
	}

	public String getID_REGISTRO() {
		return ID_REGISTRO;
	}

	public void setID_REGISTRO(String iD_REGISTRO) {
		ID_REGISTRO = iD_REGISTRO;
	}


}