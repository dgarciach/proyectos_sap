package com.sap.iot.entidad;

import java.sql.Timestamp;

public class SensorLuzMovimiento {
	
	private String G_DEVICE;
	private Timestamp G_CREATED;
	private int	C_CANTIDADLUZ, C_EXISTEMOVIMIENTO;
	
	//getters y setters
	public String getG_DEVICE() {
		return G_DEVICE;
	}
	public void setG_DEVICE(String g_DEVICE) {
		G_DEVICE = g_DEVICE;
	}
	public Timestamp getG_CREATED() {
		return G_CREATED;
	}
	public void setG_CREATED(Timestamp g_CREATED) {
		G_CREATED = g_CREATED;
	}
	public int getC_CANTIDADLUZ() {
		return C_CANTIDADLUZ;
	}
	public void setC_CANTIDADLUZ(int c_CANTIDADLUZ) {
		C_CANTIDADLUZ = c_CANTIDADLUZ;
	}
	public int getC_EXISTEMOVIMIENTO() {
		return C_EXISTEMOVIMIENTO;
	}
	public void setC_EXISTEMOVIMIENTO(int c_EXISTEMOVIMIENTO) {
		C_EXISTEMOVIMIENTO = c_EXISTEMOVIMIENTO;
	}




	
	

}
