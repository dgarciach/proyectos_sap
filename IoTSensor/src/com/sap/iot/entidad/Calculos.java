package com.sap.iot.entidad;

public class Calculos {

	private int id, tipo;
	private double ahorro, consumo;
	private String dispositivoFK;
	

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getConsumo() {
		return consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}

	public double getAhorro() {
		return ahorro;
	}

	public void setAhorro(double ahorro) {
		this.ahorro = ahorro;
	}

	public String getDispositivoFK() {
		return dispositivoFK;
	}

	public void setDispositivoFK(String dispositivoFK) {
		this.dispositivoFK = dispositivoFK;
	}

}
