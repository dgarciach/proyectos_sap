package com.sap.iot.mangedBean;

import java.sql.SQLException;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.sap.iot.entidad.*;
import com.sap.iot.servicios.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.map.MapModel;



@ManagedBean(name = "dataSensor")
@ViewScoped
public class DataSensorMB {

	private MeterGaugeChartModel GaugeTempModel;
	private MeterGaugeChartModel GaugeHumidityModel;
	private LineChartModel lineGraphic;
	private LineChartModel lineGraphicHumedad;
	private ChartSeries series1;
	private ChartSeries serieHumedad;
	private MapModel simpleModel;
	
	@ManagedProperty("#(param.nameId)")
	private String deviceName;
	
	private boolean alertaUso, aparecioAlerta, stop;
	private DataBaseOperations dataBaseOperations;
	private static Logger logger = LogManager.getLogger(DataSensorMB.class.getName());
	
	List<Number> datos = new ArrayList<Number>();


	public DataSensorMB() {
		

	}
	
	//metodo que inicializa las graficas y la conexion a base de datos
	@PostConstruct
	public void init() {

		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dataSource2");
			alertaUso = false;
			setAparecioAlerta(false);
			dataBaseOperations = new DataBaseOperations(ds);
			createGraphicTemp();
			createGraphicHumidity();
			createGaugeTemperature();
			createGaugeHumidity();
			
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			
			logger.log(Level.ERROR, "Error" + sqlException.getMessage());
		} catch (NamingException enException) {
			enException.printStackTrace();
			logger.log(Level.ERROR, "Error al buscar conexion " + enException.getMessage());
		}
	}

	// retorna lista de los datos almacenados en la tabla
	public List<Entidad> getData() {
		logger.info("Prueba mensage");
		try {
			return dataBaseOperations.getData();
			
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error en getData(): " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	// trae la data almacenada en el la clase DataBaseOperations en el metodo getTemperatura

	public MeterGaugeChartModel getGaugeTempModel() throws Exception {

		double temp = Double.parseDouble(dataBaseOperations.getTemperatura(getDeviceName()));

		if (temp >= 27) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Mensaje", "Detalles del mensaje"));
			setAlertaUso(true);
			stop();
		} else {
			setAlertaUso(false);
		}

		GaugeTempModel.setValue(temp);
		GaugeTempModel.setTitle("Temperatura "+ temp + " °C");
		return GaugeTempModel;
	}
	//trae los datos de la clase DataBaseOperation del metodo getHumidity
	public MeterGaugeChartModel getGaugeHumidityModel() throws Exception {

		double humidity = Double.parseDouble(dataBaseOperations.getHumidity(getDeviceName()));
		GaugeHumidityModel.setValue(humidity);
		GaugeHumidityModel.setTitle("Humedad " + humidity + " %");
		return GaugeHumidityModel;
	}

	public void stop() {
		if (isAlertaUso()) {
			setStop(true);
		}
	}
	
//crea el grafico de temperatura
	private void createGaugeTemperature() {
		List<Number> intervals = new ArrayList<Number>() {

			private static final long serialVersionUID = 1L;

			{
				add(10);
				add(20);
				add(30);
				add(40);
			}
		};
		GaugeTempModel = new MeterGaugeChartModel(10, intervals);
		GaugeTempModel.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
		GaugeTempModel.setIntervalOuterRadius(100);
	}
	
//crea el grafico humedad
	private void createGaugeHumidity() {
		List<Number> intervals = new ArrayList<Number>() {

			private static final long serialVersionUID = 1L;

			{
				add(20);
				add(45);
				add(60);
				add(100);
			}
		};
		GaugeHumidityModel = new MeterGaugeChartModel(10, intervals);
		GaugeHumidityModel.setTitle("Humedad");
		GaugeHumidityModel.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
		GaugeHumidityModel.setIntervalOuterRadius(100);
	}

	//crea el grafico lineal de la temperatura
	public void createGraphicTemp(){
		
		lineGraphic = new LineChartModel();
		series1 = new ChartSeries();
		series1.setLabel("Grados °C");
		
		lineGraphic.addSeries(series1);
		
		lineGraphic.setTitle("Temperatura / hora");
		lineGraphic.setLegendPosition("e");
		lineGraphic.setAnimate(true);
		lineGraphic.setShadow(true);
        lineGraphic.getAxes().put(AxisType.X, new CategoryAxis("Horas"));
        
        Axis ejeY = lineGraphic.getAxis(AxisType.Y);
        ejeY.setLabel("Temperatura");
        ejeY.setMin(0);
        ejeY.setMax(50);
        
        Axis ejeX = lineGraphic.getAxis(AxisType.X);
        ejeX.setLabel("Hora/Dia");
        ejeX.setTickAngle(-50);
        ejeX.setMin(0);
        ejeX.setMax(24);
	}
	//crea el grafico lineal de la humeedad
	public void createGraphicHumidity(){
		
		lineGraphicHumedad = new LineChartModel();
		serieHumedad = new ChartSeries();
		serieHumedad.setLabel("% Humedad");
		
		lineGraphicHumedad.addSeries(serieHumedad);
		lineGraphicHumedad.setTitle("Humedad / hora");
		lineGraphicHumedad.setLegendPosition("e");
		lineGraphicHumedad.setAnimate(true);
		lineGraphicHumedad.setShadow(true);
		lineGraphicHumedad.getAxes().put(AxisType.X, new CategoryAxis("Horas"));
        
        Axis ejeY = lineGraphicHumedad.getAxis(AxisType.Y);
        ejeY.setLabel("Humedad");
        ejeY.setMin(0);
        ejeY.setMax(100);
        System.out.println("ha llegado");
        Axis ejeX = lineGraphicHumedad.getAxis(AxisType.X);
        ejeX.setLabel("Hora/Dia");
        ejeX.setTickAngle(-50);
	}
	
	//trae los datos guardados en el metodo getTemperatureByHour correspondiente al dispositivo y la envia al grafico lineal
	public LineChartModel getLineGraphic(){
		 
         try {
			List<Entidad> lista =dataBaseOperations.getTemperatureByHour(getDeviceName());
		
			for (Entidad data : lista) {
				series1.set(data.getHora(), Double.parseDouble(data.getC_TEMPERATURA()));
			}
         
         } catch (Exception e) {
        	 logger.log(Level.ERROR, "Error en getLineGraphic): " + e.getMessage());
			e.printStackTrace();
		 }
	     return lineGraphic;
	}	
	
	//trae los datos guardados en el metodo getHumidityByHour correspondiente al dispositivo y la envia al grafico lineal
	public LineChartModel getLineGraphicHumedad(){
		 
        try {
			List<Entidad> lista =dataBaseOperations.getHumidityByHour(getDeviceName());
		
			for (Entidad data : lista) {
				serieHumedad.set(data.getHora(), Double.parseDouble(data.getC_HUMEDAD()));
			}
        
        } catch (Exception e) {
        	logger.log(Level.ERROR, "Error en getLineGraphicHumedad(): " + e.getMessage());
			e.printStackTrace();
		 }
	     return lineGraphicHumedad;
	}	

	//recive la accion desencadenada en la vista para enviar el email, usando como objeto la clase Email
	public void sendEmail(ActionEvent actionEvent) {

		Email email = new Email();
		FacesMessage message;

		if (email.createPropsEmail()) {
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email enviado correctamente", null);
		} else {
			message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: El email no se pudo enviar", null);
		}
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	// getters y setters

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public boolean isAlertaUso() {
		return alertaUso;
	}

	public void setAlertaUso(boolean alertaUso) {
		this.alertaUso = alertaUso;
	}

	public boolean isAparecioAlerta() {
		return aparecioAlerta;
	}

	public void setAparecioAlerta(boolean aparecioAlerta) {
		this.aparecioAlerta = aparecioAlerta;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public void setLineGraphic(LineChartModel lineGraphic) {
		this.lineGraphic = lineGraphic;
	}
	
	public void setLineGraphicHumedad(LineChartModel lineGraphicHumedad) {
		this.lineGraphicHumedad = lineGraphicHumedad;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
}
