package com.sap.iot.mangedBean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.sap.iot.entidad.Dispositivos;
import com.sap.iot.servicios.DataBaseOperations;

@ManagedBean(name = "ubicacionMB")
@ViewScoped
public class UbicacionMB {

	private MapModel advancedModel;

	private Marker marker;

	@PostConstruct
	public void init() {
		crearMapa();
		//crearMapaUnico();
	}

	public void crearMapa() {
		
		try {
			
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dataSource2");
			DataBaseOperations oData = new DataBaseOperations(ds);
			
			advancedModel = new DefaultMapModel();
			
			List<Dispositivos> list=oData.getDataDispositivo();
			
			// creacion de las cordenadas
			
			for (Dispositivos dispositivo : list) {
			
				LatLng coord1 = new LatLng(Double.parseDouble(dispositivo.getLATITUD()), Double.parseDouble(dispositivo.getLONGITUD()));
				// creacion de los marcadores
				advancedModel.addOverlay(new Marker(coord1, dispositivo.getID()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	public MapModel getAdvancedModel() {
		return advancedModel;
	}
	

	public void onMarkerSelect(OverlaySelectEvent event) {
		marker = (Marker) event.getOverlay();
		marker.getTitle();
	}

	public Marker getMarker() {
		return marker;
	}

}
