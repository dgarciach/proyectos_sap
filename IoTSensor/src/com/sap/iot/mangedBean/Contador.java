package com.sap.iot.mangedBean;

import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.*;
import org.apache.log4j.Logger;

public class Contador extends TimerTask {

	static Logger logger = LogManager.getLogger(Contador.class);
	int segundos;
	Timer timer = new Timer();

	@Override
	public void  run() {
				segundos++;
		
	}

	public void Contar() {
		this.segundos = 0;

		timer.schedule(new Contador(), 0, 1000);
		System.out.println("===================================Contador");

	}
	
 public int	retornarTiempo(){

	 return this.segundos;
 }

	public void cancelar() {
		timer.cancel();
	}
}
