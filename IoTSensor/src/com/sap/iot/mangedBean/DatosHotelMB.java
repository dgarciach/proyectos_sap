package com.sap.iot.mangedBean;

import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;

import com.sap.iot.entidad.Calculos;
import com.sap.iot.servicios.DataBaseOperations;

@ManagedBean(name = "dataSensorHotel")
@ViewScoped
public class DatosHotelMB {

	/*
	 * @ManagedProperty("#(param.nameId)") private String deviceName;
	 */

	private MeterGaugeChartModel gaugeHotelModel;
	private MeterGaugeChartModel gaugeHotelModelCalculo;
	private BarChartModel barModel;
	private DataBaseOperations dataBaseOperations;

	private double consumo, ahorro;
	private Calculos calcularConsumo;
	private Calculos calcularAhorro;
	private boolean estado;

	private static final Logger logger = LogManager.getLogger(DatosHotelMB.class.getName());

	public DatosHotelMB() {

	}

	@PostConstruct
	public void init() {

		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dataSource2");

			dataBaseOperations = new DataBaseOperations(ds);
			createGaugeHotel();
			estado = false;
			createGaugeCalculo();
			createBarModel();
			// backgroundColor("white");
			logger.log(Level.INFO, "Info logger");

		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			logger.log(Level.WARN, "Error" + sqlException.getMessage());
		} catch (NamingException enException) {
			enException.printStackTrace();
			logger.log(Level.WARN, "Error al buscar conexion " + enException.getMessage());
		}
	}

	// trae los datos de la clase DataBaseOperation del metodo getDataLuz
	public MeterGaugeChartModel getGaugeHotelModel() throws Exception {

		int var = 0;
		int luz = Integer.parseInt(dataBaseOperations.getDataLuz());
		System.out.println(luz);

		int res = luz / 10;
		gaugeHotelModel.setValue(res);
		gaugeHotelModel.setTitle("Nivel de Oscuridad " + res + " %");

		calcularConsumo = dataBaseOperations.getConsumo();
		calcularAhorro = dataBaseOperations.getAhorro();

		/*
		 * if (res > 20 && (int) gaugeHotelModelCalculo.getValue() == 100) {
		 * cal.setConsumo(calcularConsumo.getConsumo() + 60);
		 * cal.setAhorro(calcularAhorro.getAhorro() + 0);
		 * dataBaseOperations.actualizarConsumoAhorro(cal); }
		 */

		if (res <= 20) {
			var = 100;

			// cont.Contar();
			gaugeHotelModelCalculo.setValue(var);
			gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var + "%");
			
		}

		if (res >= 21 && res <= 40) {
			var = 70;
			gaugeHotelModelCalculo.setValue(var);
			gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var + "%");
			
		}

		if (res >= 41 && res <= 60) {
			var = 50;
			gaugeHotelModelCalculo.setValue(var);
			gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var + "%");
	
		}

		if (res >= 61 && res <= 80) {
			var = 30;
			gaugeHotelModelCalculo.setValue(var);
			gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var + "%");
			
		}

		if (res >= 81 && res <= 110) {
			var = 10;
			gaugeHotelModelCalculo.setValue(var);
			gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var + "%");
		}

		return gaugeHotelModel;
	}

	public MeterGaugeChartModel getGaugeHotelCal(int var) throws NumberFormatException, SQLException {
		gaugeHotelModelCalculo.setValue(var);
		gaugeHotelModelCalculo.setTitle("Nivel de ahorro " + var);

		return gaugeHotelModelCalculo;

	}

	// crea el grafico lineal

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();
		
		 Date utilDate = new Date(); //fecha actual
		  long lnMilisegundos = utilDate.getTime();
		  Date sqlDate = new Date(lnMilisegundos);
		  Time sqlTime = new Time(lnMilisegundos);

		try {
			Calculos lista = dataBaseOperations.getAhorro();
			Calculos listaConsumo = dataBaseOperations.getConsumo();

			ChartSeries ahorro = new ChartSeries();
			ahorro.setLabel("Ahorro");

			
			ahorro.set(sqlDate, lista.getAhorro());
		

			ChartSeries consumo = new ChartSeries();
			consumo.setLabel("Consumo");

			consumo.set(sqlDate, listaConsumo.getConsumo());
			
			model.addSeries(ahorro);
			model.addSeries(consumo);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}

	// crea el modelo de luz
	private void createGaugeHotel() {
		List<Number> intervals = new ArrayList<Number>() {

			private static final long serialVersionUID = 1L;

			{
				add(20);
				add(40);
				add(60);
				add(80);
				add(100);
			}
		};
		gaugeHotelModel = new MeterGaugeChartModel(0, intervals);
		gaugeHotelModel.setSeriesColors("E5E5E5,C4C4C4,B0B0B0,878787,5E5E5E");
		gaugeHotelModel.setIntervalOuterRadius(100);
	}

	private void createGaugeCalculo() {
		List<Number> intervals = new ArrayList<Number>() {

			private static final long serialVersionUID = 1L;

			{
				add(20);
				add(40);
				add(60);
				add(80);
				add(100);
			}
		};
		gaugeHotelModelCalculo = new MeterGaugeChartModel(100, intervals);
		gaugeHotelModelCalculo.setSeriesColors("E5E5E5,C4C4C4,B0B0B0,878787,5E5E5E");
		gaugeHotelModelCalculo.setIntervalOuterRadius(100);
	}

	private void createBarModel() {
		
		barModel = initBarModel();

		barModel.setTitle("Relacion consumo/ahorro");
		barModel.setLegendPosition("e");

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("W/h");

		Axis xAxis = barModel.getAxis(AxisType.X);
		xAxis.setLabel("Dia");
		xAxis.setMin(0);
		xAxis.setMax(500);
	}


	// GETTERS Y SETTERS

	public MeterGaugeChartModel getGaugeHotelSimpleModel() {
		return gaugeHotelModel;
	}

	public void setGaugeHotelModel(MeterGaugeChartModel GaugeHotelModel) {
		gaugeHotelModel = GaugeHotelModel;
	}

	public MeterGaugeChartModel getGaugeHotelCalModel() {
		return gaugeHotelModelCalculo;
	}

	public void setGaugeHotelCalModel(MeterGaugeChartModel GaugeHotelModelCalculo) {
		gaugeHotelModelCalculo = GaugeHotelModelCalculo;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public double getAhorro() {
		return ahorro;
	}

	public void setAhorro(double ahorro) {
		this.ahorro = ahorro;
	}

	public double getConsumo() {
		return consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Calculos getCalcularConsumo() {
		return calcularConsumo;
	}

	public void setCalcularConsumo(Calculos calcularConsumo) {
		this.calcularConsumo = calcularConsumo;
	}

	public Calculos getCalcularAhorro() {
		return calcularAhorro;
	}

	public void setCalcularAhorro(Calculos calcularAhorro) {
		this.calcularAhorro = calcularAhorro;
	}


}
