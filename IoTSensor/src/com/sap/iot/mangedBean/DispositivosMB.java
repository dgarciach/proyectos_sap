package com.sap.iot.mangedBean;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import com.sap.iot.entidad.Dispositivos;
import com.sap.iot.servicios.DataBaseOperations;

@ManagedBean(name = "dispositivos")
@SessionScoped
public class DispositivosMB {

	private DataBaseOperations dataBaseOperations;
	private List<Dispositivos> device;
	private Dispositivos cambiarEstado;
	private Dispositivos deviceName;
	private Dispositivos selectedDevice;

	private String id, latitud, longitud;
	private static Logger logger = LogManager.getLogger(Dispositivos.class.getName());

	public DispositivosMB() {

	}

	@PostConstruct
	public void init() {

		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dataSource2");
			dataBaseOperations = new DataBaseOperations(ds);
			setDevice(dataBaseOperations.getDataDispositivo());

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al ininicializar" + e.getStackTrace());
		}
	}

	public void agregar() {

		try {
			Dispositivos newDispositivo = new Dispositivos();
			newDispositivo.setID(getId());
			newDispositivo.setLATITUD(getLatitud());
			newDispositivo.setLONGITUD(getLongitud());

			dataBaseOperations.agregarDispositivo(newDispositivo);

			FacesMessage msg = new FacesMessage("Dispositivo agregado ", (getId()));
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Error al agregar" + e.getStackTrace());
		}
	}

	public void onRowEdit(RowEditEvent event) {

		try {
			Dispositivos dispositivo = (Dispositivos) event.getObject();

			dataBaseOperations.editar(dispositivo);
			FacesMessage msg = new FacesMessage("Dispositivo editado ", (dispositivo.getID()));
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error al editar" + e.getStackTrace());
		}
	}

	public void eliminarDevice()  {

		//Dispositivos dispositivo = new Dispositivos();
		
		try {
			
			cambiarEstado = dataBaseOperations.getEstado(selectedDevice.getID());

			if (cambiarEstado.getESTADO() == "TRUE") {
				cambiarEstado.setESTADO("FALSE");
				dataBaseOperations.eliminarDispositivo(cambiarEstado);
			} else {
				FacesMessage msg = new FacesMessage("Dispositivo ", (cambiarEstado.getID()) + id + "No existe.");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception e) {

			logger.log(Level.ERROR, "Error al eliminar dispositivo" + getDevice().toString() + e.getMessage());
		}

	}

	public List<Dispositivos> getDevice() {
		return device;
	}

	public void setDevice(List<Dispositivos> device) {
		this.device = device;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public Dispositivos getcCmbiarEstadoe() {
		return cambiarEstado;
	}

	public void setCambiarEstado(Dispositivos cambiarEstado) {
		this.cambiarEstado = cambiarEstado;
	}

	public Dispositivos getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(Dispositivos deviceName) {
		this.deviceName = deviceName;
	}

	public Dispositivos getSelectedDevice() {
		return selectedDevice;
	}

	public void setSelectedDevice(Dispositivos selectedDevice) {
		this.selectedDevice = selectedDevice;
	}

}
