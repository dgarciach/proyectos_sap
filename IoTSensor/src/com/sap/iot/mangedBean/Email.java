package com.sap.iot.mangedBean;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {

	public boolean createPropsEmail(){
	
		String email= "seidor.prueba@gmail.com";
		String pass = "Seidor_2018";
		
	    Properties props = System.getProperties();
	    props.put("mail.smtp.starttls.enable", true); 
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.user", email);
	    props.put("mail.smtp.password", pass);
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", true);
	    
	    Session session = Session.getInstance(props, null);
	    return createEmail(session, email, pass);
	}
	
	public boolean createEmail(Session session, String email, String pass) {

    	try
	    {
	      MimeMessage msg = new MimeMessage(session);
	      //cabecera del email
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");
	      
	      msg.setFrom(new InternetAddress(email, "Seidor Colombia"));
	      msg.setReplyTo(InternetAddress.parse("dsierra@seidor.com", false));
	      msg.setSubject("Alerta incremento de temperatura", "UTF-8");
	      msg.setText("El sistema ha detectado un aumento de la temperatura superior a 25°C. ¡se consideran acciones!...", "UTF-8");
	      msg.setSentDate(new Date());
	      
	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("dsierra@seidor.com", false));
	      Transport transport = session.getTransport("smtp");
	      transport.connect("smtp.gmail.com", email, pass);
	      transport.sendMessage(msg, msg.getAllRecipients());
    	  return true;
    	  
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       return false;
	    }
	}
}
