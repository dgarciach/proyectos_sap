import urllib3 #Esta libreria permite invocar un servicio externo
import certifi #Esta libreria permite gestionar la seguridad

http = urllib3.PoolManager(
    cert_reqs='CERT_REQUIRED', #Forzar el chekeo del certificado
    ca_certs=certifi.where(),
)

#URL metodo post servicio IOT de sap SCP
url = 'https://iotmmsp2000160740trial.hanatrial.ondemand.com/com.sap.iotservices.mms/v1/api/http/data/939dcaab-4ca2-4641-a689-7e879bc483cc';

#Encabezado del mensaje


headers = urllib3.util.make_headers() #Creacion de HEADERS en nuestro servicio web IOT SAP.
headers['Authorization'] = 'Bearer ceefc014fe36ca37779bd05d34a579e5' # Seguridad TOKEN de SAP IOT asignado para el dispositivo
headers['Content-Type'] = 'application/json;charset=utf-8'          #Contenido a enviar mediante POST

#Cuerpo del mensaje, datos recopilados por el sensor.

body = '{"mode":"sync",	"messageType":"5db1a99fe6adc226f05c", "messages\":[{\"temperatura\":'+arrayArduinoLine[2]+',\"humedad\":'+arrayArduinoLine[0]+'}]}'

#Invocacion del servicio IOT con las configuraciones realizadas
try:
    r = http.urlopen('POST',url, body=body, headers=headers)
    print(r.status)
    print(r.data)
except urllib3.exceptions.SSLError as e:
    print e
