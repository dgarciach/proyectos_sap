import requests # http://docs.python-requests.org/en/master/
import serial
arduino = serial.Serial('COM3', 9600)
msgtypeid = '56ddbb15c3d7fad6e138'
#authtoken = 'c555c2ac937f9d49fbb126a779e4b87c'
#authtoken = 'ceefc014fe36ca37779bd05d34a579e5'     


def postiotneo ():
	
	rawString = arduino.readline()
	print("Arduino line: "+ rawString)
	
	#[0] = humidity, [1]= idDevice, [2]= temperature, [3]=tocken
	arrayArduinoLine= rawString.split()
	url = "https://iotmmsp2000160740trial.hanatrial.ondemand.com/com.sap.iotservices.mms/v1/api/http/data/"+arrayArduinoLine[1]
	payload = "{\"mode\":\"sync\",\"messageType\":\""+msgtypeid+"\",\"messages\":[{\"temperatura\":"+arrayArduinoLine[2]+",\"humedad\":"+arrayArduinoLine[0]+"}]}"

	
	headers = {
			'content-type': "application/json",
			'authorization': "Bearer "+ arrayArduinoLine[3],
			'cache-control': "no-cache"
			}

	print("Payload to post: ", payload)

	response = requests.request("POST", url, data=payload, headers=headers)

	print(response.status_code, response.text)

	return

try:
	while(True):		
		postiotneo()
		
except KeyboardInterrupt:
	print("Detenido por el usuario")






arduino.close()
