package com.sap.db.managedBean;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import com.sap.db.Entidades.DataEntity;
import com.sap.db.servicios.*;
import org.primefaces.model.chart.MeterGaugeChartModel;

@ManagedBean(name = "data")
@ViewScoped
public class DataMB {

	private String G_DEVICE, C_CPU_USAGE, C_CPU_TYPE, C__TIME;
	private Timestamp G_CREATED;
	private MeterGaugeChartModel meterGaugeModel;
	private boolean alertaUso;

	private DataBaseOperations dataBaseOperations;
	static final Logger LOGGER = Logger.getLogger(DataMB.class.getName());
	List<Number> datos = new ArrayList<Number>();

	// inicializa el metodo que crea el grafico

	public DataMB() {
		createMeterGaugeModel();

	}

	@PostConstruct
	public void init() {
		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dataSource1");
			alertaUso = false;
			dataBaseOperations = new DataBaseOperations(ds);

		} catch (SQLException sqlException) {
			LOGGER.log(Level.WARNING, "Error" + sqlException.getMessage());
		} catch (NamingException enException) {
			LOGGER.log(Level.WARNING, "Error al buscar conexion " + enException.getMessage());
		}
	}

	// retorna lista de los datos almacenados en la tabla
	public List<DataEntity> getData() {
		System.out.println("llego a getData()");
		try {
			return dataBaseOperations.getData();

		} catch (Exception e) {
			System.out.println("Error en getData(): " + e.getMessage());
		}
		return null;

	}

	// trae la data almacenada en el la clase DataBaseOperations en el metodo

	public MeterGaugeChartModel getMeterGaugeModel() throws Exception {

		String dato = dataBaseOperations.getCpuUsage();
		double cpu_usage = Double.parseDouble(dato);

		if (cpu_usage > 50) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Message", "Additional Message Detail"));
			setAlertaUso(true);
		}

		meterGaugeModel.setValue(cpu_usage);

		return meterGaugeModel;
	}

	private void createMeterGaugeModel() {
		List<Number> intervals = new ArrayList<Number>() {

			private static final long serialVersionUID = 1L;

			{
				add(25);
				add(50);
				add(75);
				add(100);
			}
		};
		meterGaugeModel = new MeterGaugeChartModel(0, intervals);
		meterGaugeModel.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
	}

	public void sendEmail(ActionEvent actionEvent) {

		Email email = new Email();
		FacesMessage message;

		if (email.createPropsEmail()) {
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email enviado correctamente", null);
		} else {
			message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: El email no se pudo enviar", null);
		}
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	// getters y setters
	public String getC_CPU_TYPE() {
		return C_CPU_TYPE;
	}

	public void setC_CPU_TYPE(String c_CPU_TYPE) {
		C_CPU_TYPE = c_CPU_TYPE;
	}

	public String getC__TIME() {
		return C__TIME;
	}

	public void setC__TIME(String c__TIME) {
		C__TIME = c__TIME;
	}

	public String getG_DEVICE() {
		return G_DEVICE;
	}

	public void setG_DEVICE(String g_DEVICE) {
		G_DEVICE = g_DEVICE;
	}

	public String getC_CPU_USAGE() {
		return C_CPU_USAGE;
	}

	public void setC_CPU_USAGE(String c_CPU_USAGE) {
		C_CPU_USAGE = c_CPU_USAGE;
	}

	public Timestamp getG_CREATED() {
		return G_CREATED;
	}

	public void setG_CREATED(Timestamp g_CREATED) {
		G_CREATED = g_CREATED;
	}

	public boolean isAlertaUso() {
		return alertaUso;
	}

	public void setAlertaUso(boolean alertaUso) {
		this.alertaUso = alertaUso;
	}

}
