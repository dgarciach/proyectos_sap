package com.sap.db.Entidades;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-06-14T11:05:34.114-0500")
@StaticMetamodel(DataEntity.class)
public class DataEntity_ {
	public static volatile SingularAttribute<DataEntity, String> C__TIME;
	public static volatile SingularAttribute<DataEntity, String> C_CPU_TYPE;
	public static volatile SingularAttribute<DataEntity, String> C_CPU_USAGE;
	public static volatile SingularAttribute<DataEntity, Timestamp> G_CREATED;
	public static volatile SingularAttribute<DataEntity, String> G_DEVICE;
	public static volatile SingularAttribute<DataEntity, Integer> ID_PRUEBA;
}
