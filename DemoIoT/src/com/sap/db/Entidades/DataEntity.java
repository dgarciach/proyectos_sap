package com.sap.db.Entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(schema = "SYSTEM", name = "T_IOT_C89350671AC8E1C4EC17")
@NamedQueries({ @NamedQuery(name = "DataEntity.findAll", query = "SELECT d FROM DataEntity d"),
@NamedQuery(name = "DataEntity.findByCpuUsage", query = "SELECT d FROM DataEntity d WHERE d.C_CPU_USAGE = :C_CPU_USAGE") })

public class DataEntity  implements Serializable{


	private static final long serialVersionUID = 1L;

	// public static final String FIND_ALL = "DataEntity.findAll";
	// public static final String FIND_BY_CPU = "DataEntity.findByCpuUsage";

	@Column(name = "C__TIME")
	private String C__TIME;

	@Column(name = "C_CPU_TYPE")
	private String C_CPU_TYPE;

	@Column(name = "C_CPU_USAGE")
	private String C_CPU_USAGE;

	@Column(name = "G_CREATED")
	private Timestamp G_CREATED;

	@Column(name = "G_DEVICE")
	private String G_DEVICE;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PRUEBA")
	private int ID_PRUEBA;
	

	
	/*private String G_DEVICE,C_CPU_USAGE,C_CPU_TYPE,C__TIME ; 
	private Timestamp G_CREATED;
	private int ID_PRUEBA;*/
	
	public DataEntity() {
	}


	public String getG_DEVICE() {
		return G_DEVICE;
	}


	public void setG_DEVICE(String g_DEVICE) {
		G_DEVICE = g_DEVICE;
	}


	public String getC__TIME() {
		return C__TIME;
	}


	public void setC__TIME(String c__TIME) {
		C__TIME = c__TIME;
	}


	public String getC_CPU_USAGE() {
		return C_CPU_USAGE;
	}


	public void setC_CPU_USAGE(String c_CPU_USAGE) {
		C_CPU_USAGE = c_CPU_USAGE;
	}


	public Timestamp getG_CREATED() {
		return G_CREATED;
	}


	public void setG_CREATED(Timestamp g_CREATED) {
		G_CREATED = g_CREATED;
	}


	public String getC_CPU_TYPE() {
		return C_CPU_TYPE;
	}


	public void setC_CPU_TYPE(String c_CPU_TYPE) {
		C_CPU_TYPE = c_CPU_TYPE;
	}


	public int getID_PRUEBA() {
		return ID_PRUEBA;
	}


	public void setID_PRUEBA(int iD_PRUEBA) {
		ID_PRUEBA = iD_PRUEBA;
	}

}