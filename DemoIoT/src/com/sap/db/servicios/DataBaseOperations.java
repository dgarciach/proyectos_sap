package com.sap.db.servicios;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;
import javax.sql.DataSource;

import com.sap.db.Entidades.*;
import com.sap.db.managedBean.DataMB;

public class DataBaseOperations {

	/*
	 * private static final String DRIVER = "com.sap.db.jdbc.Driver"; // jdbc
	 * 4.0 private static final String URL =
	 * "jdbc:sap://localhost:30015?reconnect=true";
	 */
	static final Logger LOGGER = Logger.getLogger(DataBaseOperations.class.getName());
	private DataSource dataSource;

	// acceso a los datos que encapsula el JDBC
	public DataBaseOperations(DataSource newDataSource) throws SQLException {
		setDataSource(newDataSource);
	}

	// Establecer el origen de datos que se utilizará para las operaciones de la
	// base de datos.
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource newDataSource) throws SQLException {
		this.dataSource = newDataSource;
		verificarTabla();
	}

	// agregar dato a la tabla

	public void agregarData(DataEntity data) throws SQLException {

		String sql = "insert into T_IOT_C89350671AC8E1C4EC17 values(?,?,?,?,?)";
		Connection conexion = dataSource.getConnection();
		try {

			PreparedStatement preparedStatement = conexion.prepareStatement(sql);
			preparedStatement.setString(0, data.getG_DEVICE());
			preparedStatement.setTimestamp(1, data.getG_CREATED());
			preparedStatement.setString(2, data.getC_CPU_USAGE());
			preparedStatement.setString(3, data.getC_CPU_TYPE());
			preparedStatement.setString(4, data.getC__TIME());
			preparedStatement.executeUpdate();

		} finally {
			if (conexion != null) {
				conexion.close();
			}
		}

	}

	/*
	 * "G_DEVICE", "G_CREATED", "C_CPU_USAGE", "C_CPU_TYPE", "C__TIME"
	 */

	// traer todos los datos de la tabla
	public List<DataEntity> getData() throws Exception {

		String sql = "select * from T_IOT_C89350671AC8E1C4EC17";
		Connection con = dataSource.getConnection();
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			// ResultSetMetaData rsmd = rs.getMetaData();
			ArrayList<DataEntity> listCPU = new ArrayList<DataEntity>();
			// int numColum = rsmd.getColumnCount();
			while (rs.next()) {
				DataEntity listaData = new DataEntity();
				
		
				listaData.setG_DEVICE(rs.getString(1));
				listaData.setG_CREATED(rs.getTimestamp(2));
				listaData.setC_CPU_USAGE(rs.getString(3));
				listaData.setC_CPU_TYPE(rs.getString(4));
				listaData.setC__TIME(rs.getString(5));
				listCPU.add(listaData);
				System.out.println("|| "+listCPU+" ||\n");	
			}
			return listCPU;
			
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	// obtener datos la cpu
	public String getCpuUsage() throws Exception {

		String sql = "select TOP 1 C_CPU_USAGE from T_IOT_C89350671AC8E1C4EC17 order by G_CREATED desc";
		Connection con = dataSource.getConnection();

		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				
				return rs.getString(1);
			}else{
				return "0";
			}
			
			
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	// Comprobar si la tabla ya existe y créarla si no.

	private void verificarTabla() throws SQLException {
		Connection conexion = null;

		try {
			conexion = dataSource.getConnection();
			if (!existeTabla(conexion)) {
				crearTabla(conexion);
			}
		} finally {
			if (conexion != null) {
				conexion.close();
			}
		}
	}

	// Verificar si la tabla ya existe.

	private boolean existeTabla(Connection conn) throws SQLException {
		DatabaseMetaData meta = conn.getMetaData();
		ResultSet rs = meta.getTables(null, null, "T_IOT_C89350671AC8E1C4EC17", null);
		while (rs.next()) {
			String name = rs.getString("TABLE_NAME");
			if (name.equals("T_IOT_769BB7B00F0D3AA571D4")) {
				return true;
			}
		}
		return false;
	}

	// crear tabla

	private void crearTabla(Connection connection) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement("CREATE TABLE T_IOT_C89350671AC8E1C4EC17 "
				+ "(ID VARCHAR(255) PRIMARY KEY, " + "G_DEVICE VARCHAR (30)," + "G_CREATED VARCHAR (30)"
				+ "C_CPU_USAGE VARCHAR (30)" + "C_CPU_TYPE VARCHAR (30)" + "C__TIME VARCHAR(30))");
		pstmt.executeUpdate();
	}

	/*
	 * private Connection getConnection(String driver, String url, String user,
	 * String password) throws Exception { Class.forName(driver); return
	 * DriverManager.getConnection(url, user, password);
	 * 
	 * }
	 * 
	 * // cierra la conexion public void destroy(Connection con, Statement stmt)
	 * throws SQLException { if (stmt != null) { stmt.close(); } if (con !=
	 * null) { con.close(); } }
	 */
}
